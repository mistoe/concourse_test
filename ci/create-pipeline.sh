#!/bin/sh
export NEW_VERSIONS=$(cat branches/branches)
export OLD_VERSIONS=$(cat branches/removed)

echo "Found new branches $NEW_VERSIONS"
echo "Remove old branches $OLD_VERSIONS"

echo "Execute fly login -t serenity -c https://ci.sys.emea.vwapps.io/ -u $concourseUsername -p <hidden> -n newTeam"
fly login -t serenity -c https://ci.sys.emea.vwapps.io/ -u $concourseUsername -p $concoursePassword -n newTeam
fly sync -t serenity

for version in $NEW_VERSIONS; do
#   sed "s/___BRANCH___/$version/g" git-branches/ci/build-pipeline.tmpl > git-branches/ci/pipeline-app.result
	echo "Create pipeline branch $version"
#   fly -t serenity sp -n -p your-hello-world-$version -c git-branches/ci/pipeline-app.result
	echo "Unpause pipeline branch $version"
#   fly -t serenity up -p your-hello-world-$version
done
for version in $OLD_VERSIONS; do
   echo "Delete pipeline branch $version"
#   fly -t serenity dp -n -p your-hello-world-$version
done